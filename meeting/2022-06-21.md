# Roll call: who's there and emergencies

 * anarcat
 * gaba
 * lavamind

We had two emergencies, both incidents were resolved in the morning:

 * [failing CI jobs][]
 * [failed disk on fsn-node-01][]
 
 [failing CI jobs]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/incident/40806
 [failed disk on fsn-node-01]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/incident/40805

# OKR / roadmap review

[TPA OKRs][]: roughly 19% done

* [mail services][]: 20%. TPA-RFC-15 was rejected, we're going to go
  external, need to draft TPA-RFC-31
* [Retirements][]: 20%. no progress foreseen before end of quarter
* [codebase cleanup][]: 6%. often gets pushed to the side by
  emergencies, lots of good work done to update Puppet to the latest
  version in Debian, see https://wiki.debian.org/Teams/Puppet/Work
* [Bullseye upgrades][]: 48%. still promising, hoping to finish
  by end of summer!
* [High-performance cluster][]: 0%. no grant, nothing moving for now,
  but at least it's on the fundraising radar

[Web OKRs][]: 42% done overall!

* The donate OKR: is about 25% complete still, to start in next quarter
* Translation OKR: still done
* Docs OKR: no change since last meeting:
  * dev.tpo work hasn't started yet, might be possible to start
    depending on kez availability? @gaba needs to call for a meeting,
    followup in [tpo/web/dev#6][]
  * documentation improvement might be good for hack week

[TPA OKRs]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2022
[mail services]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/4
[retirements]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/1
[codebase cleanup]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/3
[bullseye upgrades]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/5
[High-performance cluster]: https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/2
[web OKRs]: https://gitlab.torproject.org/tpo/web/team/-/wikis/roadmap/2022
[tpo/web/dev#6]: https://gitlab.torproject.org/tpo/web/dev/-/issues/6

# Dashboard review

We looked at the team dashboards:

 * https://gitlab.torproject.org/tpo/tpa/team/-/boards/117
 * https://gitlab.torproject.org/groups/tpo/web/-/boards
 * https://gitlab.torproject.org/groups/tpo/tpa/-/boards

... and per user dashboards:

 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=anarcat
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=kez
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=lavamind

Things seem to be well aligned for the vacations. We put in "backlog"
the things that will *not* happen in June.

# Vacation planning

Let's plan 1:1 and meetings for july and august.

Let's try to schedule 1:1s during the 2 week where anarcat is
available, anarcat will arrange those by email. he will also schedule
the meetings that way.

We'll work on a plan for Q3 in mid-july, gaba will clean the web
board. In the meantime, we're in "vacation mode" until anarcat comes
back from vacation, which means we mostly deal with support requests
and emergencies, along with small projects that are already started.

# Icinga vs Prometheus

anarcat presented a preliminary draft of TPA-RFC-33, presenting the
background, history, current setup, and requirements of the monitoring
system.

lavamind will take some time to digest it and suggest changes. No
further work is expected to happen on monitoring for a few weeks at
least.

# Other discussions

We should review the Icinga vs Prometheus discussion at the next
meeting. We also need to setup a new set of OKRs for Q3/Q4 or at least
prioritize Q3 at the next meeting.

# Next meeting

Some time in July, to be determined.

# Metrics of the month

N/A we're not at the end of the month yet.

# Ticket filing star of the month

It has been suggested that people creating a lot of tickets in our
issue trackers are "annoying". We strongly deny those claims and
instead propose we spend some time creating a mechanism to determine
the "ticket filing star" of the month, the person who will have filed
the most (valid) tickets with us in the previous month.

Right now, this is pretty hard to extract from GitLab, so it will
require a little bit of wrangling with the GitLab API, but it's a
simple enough task. If no one stops anarcat, he may come up with
something like this in the Hackweek. Or something.
