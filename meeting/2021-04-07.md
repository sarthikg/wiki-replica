This is a short email to let people know that TPA meetings are suspended
for a while, as we are running under limited staff. I figured I would
still send you those delicious metrics of the month and short updates
like this to keep people informed of the latest.

# Metrics of the month

 * hosts in Puppet: 87, LDAP: 90, Prometheus exporters: 141
 * number of Apache servers monitored: 28, hits per second: 0
 * number of Nginx servers: 2, hits per second: 2, hit ratio: 0.87
 * number of self-hosted nameservers: 6, mail servers: 7
 * pending upgrades: 0, reboots: 1
 * average load: 1.04, memory available: 1.98 TiB/2.74 TiB, running processes: 569
 * bytes sent: 269.96 MB/s, received: 162.58 MB/s
 * [GitLab tickets][]: 138 tickets including...
   * open: 0
   * icebox: 106
   * backlog: 22
   * next: 7
   * doing: 4
   * (closed: 2225)

 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Note that the Apache exporter broke because of a fairly dumb error
introduced in february, so we do not have the right "hits per second"
stats there. Gory details of that bug live in:

https://github.com/voxpupuli/puppet-prometheus/pull/541

# Quote of the week

"Quoting. It's hard."

Okay, I just made that one up, but yeah, that was a silly one.
