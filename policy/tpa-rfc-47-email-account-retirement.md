---
title: TPA-RFC-47: Email account retirement
---

[[_TOC_]]

Summary: delete email accounts after a delay when a user is retired

# Background

As part of working on improving the on-boarding and off-boarding
process, we have come up with a proposal to set a policy on what
happens with user's email after they leave. A number of discussions
happened on this topic in the past, but have mostly stalled.

# Proposal

When someone is fired or leaves the core team, we setup an auto-reply
with a bounce announcing the replacement email (if any). This gives
agency to the sender, which is better entitled to determine whether
the email should be forwarded to the replacement or another contact
should be found for the user.

The auto-reply expires 12 months later, at which point the email
simply bounces with a generic error. We also remove existing forwards
older than 12 months that we already have.

# Impact

## For staff

We also encourage users to setup and use role accounts instead of
using their personal accounts for external communications. Mailing
lists, RT queues, and email forwards are available from TPA.

This implies that individual users MUST start using role accounts in
their communications as much as possible. Typically, this means having
a role account for your team and keeping it in "CC" in your
communications. For example, if John is part of the accounting team,
all his professional communications should `Cc:
accounting@torproject.org` to make sure the contacts have a way to
reach accounting if `john@torproject.org` disappears.

Users are also encouraged to use the myriad of issue trackers and
communication systems at their disposal including RT, GitLab, and
Mailman mailing lists, to avoid depending on their individual address
being available in the long term.

## For long time core contributors

Long time core contributors might be worried this proposal would
impact their future use of their `@torproject.org` email address. For
example, say Alice is one of the core contributors who's been around
for the longest, not a founder, but almost. Alice might worry that
`alice@torproject.org` might disappear if they become inactive, and
might want to start using an alternate email address in their
communications...

The rationale here is that long time core contributors are likely to
remain core contributors for a long time as well, and therefore keep
their email address for an equally long time. It *is* true, however,
that a core contributor might lose their identity if they get expelled
from the project or completely leave. This is by design: if the
person is not a contributor to the project anymore, they should not
hold an identity that allows them to present themselves as being part
of the project.

# Alternatives considered

Those are other options that were considered to solve the current
challenges with managing off-boarding users.

## Status quo

Right now, when we retire a user, their account is first "locked"
which means their access to various services is disabled. But their
email still works for 186 days (~6 months). After that date, the email
address forward is removed from servers and email bounces.

We currently let people keep their email address (and, indeed, their
LDAP account) when they resign or are laid off from TPI, as long as
they remain core contributors. Eventually, during the core membership
audit, those users *may* have their LDAP account disabled but can keep
their email address essentially forever, as we offer users to be added
to the forward alias.

For *some* users, their personal email forward is forwarded to a role
account. This is the case for some past staff, especially in
accounting.

## Dual policy

We could also *two* policies, one for core members and another for TPI
employees.

# Approval

This needs to go by tor-internal (and tor-project?).

# Deadline

TODO: deadline TBD

# Status

This proposal is currently in the `draft` state.

# References

 * [discussion ticket][]
 * [Onboarding and offboarding procedures][]

[discussion ticket]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/32558

[Onboarding and offboarding procedures]: https://gitlab.torproject.org/tpo/team/-/wikis/Onboarding-Procedures
