---
title: TPA-RFC-42: 2023 TPA Roadmap
---

[[_TOC_]]

Summary: simple roadmap for 2023.

# Background

We've used OKRs for the 2022 roadmap, and the results are mixed. On
the one hand, we had ambitious, exciting goals, but on the other hand
we completely underestimated how much work was required to accomplish
those key results. By the end of the year or so, we were not even at
50% done.

So we need to decide whether we will use this process again
for 2023. We also need to figure out how to fit "things that need to
happen anyways" inside the OKRs, or just ditch the OKRs in favor of a
regular roadmap, or have both side by side.

We also need to determine specifically what the goals for 2023 will
be.

# Proposal

## 2023 Goals

Anarcat brainstorm:

 * bookworm upgrades, this includes:
   * puppet server 7
 * mail migration (e.g. execute TPA-RFC-31)
 * cymru migration (e.g. execute TPA-RFC-40, if not already done)
 * retire gitolite/gitweb (e.g. execute TPA-RFC-36)
 * retire schleuder (e.g. execute TPA-RFC-41)
 * retire SVN (e.g. execute TPA-RFC-11)
 * deploy a Puppet CI
   * make the Puppet repo public, possibly by removing private content
     and just creating a "graft" to have a new repository without old
     history (as opposed to rewriting the entire history, because then
     we don't know if we have confidential stuff in the old history)
 * plan for summer vacations
 * self-host discourse?

### Must have

### Nice to have

### Non-Goals

# Alternatives considered

# Costs

N/A.

# Approval

TPA.

# Deadline

This should be established by the end of 2022.

# Status

This proposal is currently in the `standard` state.

# References

- [tpo/tpa/team#40924](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40924): discussion issue
- [roadmap/2022](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2022): previous roadmap
