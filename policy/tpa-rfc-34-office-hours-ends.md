---
title: TPA-RFC-34: End of TPA office hours
---

[[_TOC_]]

Summary: office hours have already ended, this note makes it official.

# Background

In September 2021, we established "office hours" as part of
[TPA-RFC-12][], to formalize the practice of occupying a Big Blue
Button (BBB) room every Monday. The goal was to help people with small
things or resolve more complex issues but also to create a more
sympathetic space than the coldness of space offered by IRC and issue
trackers.

This practice didn't last long, however. As early at December 2021, we
noted that some of us didn't really have time to tend to the office
hours or when we did, no one actually showed up. When people *would*
show up, it was generally planned in advance.

At this point, we have basically given up on the practice.

# Proposal

We formalize the end of TPA office hours. Concretely, this means
removing the "Office hours" section from [TPA-RFC-2][].

Instead, we encourage our staff to pick up the phone and just call
each other if they need to carry information or a conversation that
doesn't happen so well over other medium. This extends to all folks in
tor-internal that need our help.

The "office hours" room will remain in BBB
(<https://tor.meet.coop/ana-ycw-rfj-k8j>) but will be used on a
need-to basis. Monday is still a good day to book such appointments,
during America/Eastern or America/Pacific "business hours", depending
on who is "star of the week".

# Approval

This is assumed to be approved by TPA already, since, effectively, no
one has been doing office hours for months already.

# Deadline

This proposal is already adopted.

# Status

This proposal is currently in the `obsolete` state.

# References

 * [TPA-RFC-2][]
 * [TPA-RFC-12][]

[TPA-RFC-2]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-2-support
[TPA-RFC-12]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-12-triage-and-office-hours
