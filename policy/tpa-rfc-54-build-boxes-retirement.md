---
title: TPA-RFC-54: build-x86-05 and build-x86-06 retirement
---

[[_TOC_]]

Summary: old Jenkins build boxes are getting retired

# Background

As part of the moly retirement ([tpo/tpa/team#29974][]), we need to
retire or migrate the `build-x86-05` and `build-x86-06` machines.

Another VM on moly, `fallax` was somewhat moved into the new Ganeti
cluster (`gnt-dal`), but we're actually having trouble putting it in
production as it's refusing to convert into a proper DRBD node. We
might have to rebuild `fallax` from scratch.

No one has logged into `build-x86-05` in over 2 years according to
last(1). `build-x86-06` was used more recently by weasel, once in February
and January but before that in July.

# Proposal

Retire the `build-x86-05` and `build-x86-06` machines.

It's unclear if we'd be able to easily import the build boxes in the
new cluster, so it seems better to retire the build boxes than fight
the process to try to import them.

It seems like, anyways, whatever purpose those boxes serve would be
better served by (reproducible!) CI jobs. Alternatively, if we do want
to have such a service, it seems to me easier to rebuild them from
scratch.

# Deadline

The VMs have already been halted and the retirement procedure
started. They will be deleted from moly in 7 days and their backups
removed in 30 days.

# Status

This proposal is currently in the `obsolete` state.

# References

See [tpo/tpa/team#29974][] for the discussion issue.

[tpo/tpa/team#29974]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/29974
