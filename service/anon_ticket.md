A web application that allows users to create anonymous tickets on the Tor
Project's GitLab instance by leveraging the GitLab API.

The project is developed in-house and hosted on GitLab at
[tpo/tpa/anon_ticket](https://gitlab.torproject.org/tpo/tpa/anon_ticket/).

[[_TOC_]]

# Tutorial

<!-- simple, brainless step-by-step instructions requiring little or -->
<!-- no technical background -->

# How-to

<!-- more in-depth procedure that may require interpretation -->

## Pager playbook

<!-- information about common errors from the monitoring system and -->
<!-- how to deal with them. this should be easy to follow: think of -->
<!-- your future self, in a stressful situation, tired and hungry. -->

## Disaster recovery

If the PostgreSQL database isn't lost, see the
[installation procedure](#installation).

If having to install from scratch, see also
[anon_ticket Quickstart](https://gitlab.torproject.org/tpo/tpa/anon_ticket#11-quickstart)

# Reference

## Installation

Prerequisite for installing this service is an LDAP role account.

The service is mainly deployed via the `profile::anonticket` Puppet class,
which takes care of installing dependencies, configuring a postgresql
user/database, an nginx reverse proxy and systemd user service unit file.

A Python virtual environment must then be manually provisioned in `$HOME/.env`,
and the `ticketlobby.service` user service unit file must then be enabled and
activated.

## Upgrades

   $ source .env/bin/activate  # To activate the python virtual environment
   $ cd anon_ticket
   $ git fetch origin main
   $ git merge origin/main
   $ python manage.py migrate  # To apply new migrations
   $ python manage.py collectstatic  # To generate new `static` files
   $ systemctl --user reload/restart ticketlobby.service

## SLA

There is no SLA established for this service.

## Design and architecture

`anon_ticket` is a [Django](https://www.djangoproject.com/) application and
project. Frontend is served by gunicorn and ngnix as proxy and nginx for
`static` files. It uses TPA's [postgresql](howto/postgresql) for storage and
[Gitlab](howto/gitlab) API to create users, issues and notes on issues.

## Services

The nginx reverse proxy listens on the standard HTTP and HTTPS ports, handles
TLS termination, and forwards requests to the ticketlobby service unit that
launches gunicorn, which handles the anon_ticket Django project
(call ticketlobby) containing the application WSGI.

## Storage

Persistent data is stored in a PostgreSQL database.

## Queues

None.

## Interfaces

This service uses the [Gitlab REST API](https://docs.gitlab.com/ee/api/rest/).

The application can be managed via its Web interface or via
[Django cli](https://docs.djangoproject.com/en/3.1/ref/django-admin/)

## Authentication

standalone plus [Gitlab API tokens](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html),
see [tpo/tpa/team#41510](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41510).

## Implementation

<!-- programming languages, frameworks, versions, license -->
Python, Django >= 3.1 licensed under BSD 3-Clause "New" or "Revised" license.

## Related services

[Gitlab](howto/gitlab), [PostgreSQL](howto/postgresql), nginx

## Issues

This project has its own issue tracker at https://gitlab.torproject.org/tpo/tpa/anon_ticket/-/issues

## Maintainer

Service deployed by @lavamind, @juga and @ahf.

## Users

Any user that wish to report/comment an issue in https://gitlab.torproject.org,
without having an account.

## Upstream

Upstream are volunteers and some TPI persons, see
[Contributor analytics](https://gitlab.torproject.org/tpo/tpa/anon_ticket/-/graphs/master?ref_type=heads)

Upstream is not very active.

To report Issues, see [Issues](#issues).

## Monitoring and metrics

No known monitoring nor metrics.

To keep up to date, see [Upgrades](#upgrades).

## Tests

The service has to be tested manually, going to
https://anonticket.torproject.org and check that you can:

- `create identifier`
- `login with identifier`

  - `See a list of all projects`
  - `Search for an issue`
  - `Create an issue`
  - Create a `note` on an existing issue
  - `See My Landing Page`
- `request gitlab account`

To test the code, see
[anon_ticket Tests](https://gitlab.torproject.org/tpo/tpa/anon_ticket/-/tree/master?ref_type=heads#50-tests)

## Logs

Logs are sent to journal. Gunicorn access and error logs are also saved at
`$HOME/log` without IP (proxy's one) nor User-Agent.

## Backups

<!-- does this service need anything special in terms of backups? -->
<!-- e.g. locking a database? special recovery procedures? -->

## Other documentation

[anon_ticket README](https://gitlab.torproject.org/tpo/tpa/anon_ticket/-/blob/2fa7b0aaf5b143032f92b3df61b13ebe66d2101b/readme.MD)

# Discussion

This service was initially deployed by @ahf at https://anonticket.onionize.space/
and has been migrated here, see
[tpo/tpa/team#40577](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40577).

In the long term, this service will deprecate https://gitlab.onionize.space/
service, deployed by @ahf, from the [Gitlab Lobby](https://gitlab.torproject.org/tpo/tpa/gitlab-lobby) code, because its functionality has already been integrated in
`anon_ticket`.

## Overview

<!-- describe the overall project. should include a link to a ticket -->
<!-- that has a launch checklist -->

<!-- if this is an old project being documented, summarize the known -->
<!-- issues with the project. -->

## Security and risk assessment

<!--

 5. When was the last security review done on the project? What was
    the outcome? Are there any security issues currently? Should it
    have another security review?

 6. When was the last risk assessment done? Something that would cover
    risks from the data stored, the access required, etc.

-->

## Technical debt and next steps

<!--

 7. Are there any in-progress projects? Technical debt cleanup?
    Migrations? What state are they in? What's the urgency? What's the
    next steps?

 8. What urgent things need to be done on this project?

-->

Nothing urgent.

Next steps: [anon_ticket Issues](https://gitlab.torproject.org/tpo/tpa/anon_ticket/-/issues/?sort=created_date&state=opened&first_page_size=100)

## Proposed Solution

<!-- Link to RFC -->

## Other alternatives

<!-- include benchmarks and procedure if relevant -->
