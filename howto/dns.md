DNS is the Domain Name Service. It is what turns a name like
`www.torproject.org` in an IP address that can be routed over the
Internet. TPA maintains its own DNS servers and this document attempts
to describe how those work.

TODO: mention unbound and a rough overview of the setup here

[[_TOC_]]

# Tutorial

<!-- simple, brainless step-by-step instructions requiring little or -->
<!-- no technical background -->

# How to

Most operations on DNS happens in the `domains` repository
(`dnsadm@nevii.torproject.org:/srv/dns.torproject.org/repositories/domains`). Those zones contains
the master copy of the zone files, stored as (mostly) standard Bind zonefiles
([RFC 1034](https://tools.ietf.org/html/rfc1034)), but notably without a SOA.

Tor's DNS support is fully authenticated with DNSSEC, both to the
outside world but also internally, where all TPO hosts use DNSSEC in
their resolvers.

## Editing a zone

Zone records can be added or modified to a zone in the `domains` git
and a push. DNSSEC records are managed automatically by
`manage-dnssec-keys` in the `dns-helpers` git repository, through
a cron job in the `dnsadm` user on the master DNS server (currently
`nevii`).

Serial numbers are managed automatically by the git repository hooks.

## Adding a zone

To add a *new* zone to our infrastructure, the following procedure
must be followed:

 1. add zone in `domains` repository
    (`dnsadm@nevii.torproject.org:/srv/dns.torproject.org/repositories/domains`)
 2. add zone in the
    `modules/bind/templates/named.conf.torproject-zones.erb` Puppet
    template for DNS secondaries to pick up the zone
 3. also add IP address ranges (if it's a reverse DNS zone file) to
    `modules/torproject_org/misc/hoster.yaml` in the `tor-puppet.git`
    repository
 4. run puppet on DNS servers: `cumin 'C:roles::dns_primary or C:bind::secondary' 'puppet agent -t'`
 5. add zone to `modules/postfix/files/virtual`, unless it is a
    reverse zonefile
 6. add zone to nagios: copy an existing `DNS SOA sync` block and
    adapt
 7. add zone to external DNS secondaries (currently [Netnod](https://www.netnod.se/))
 8. make sure the zone is delegated by the root servers somehow. for
    normal zones, this involves adding our nameservers in the
    registrar's configuration. for reverse DNS, this involves asking
    our upstreams to delegate the zone to our DNS servers.

Note that this is a somewhat rarer procedure: this happens only when a
completely new [domain name](https://en.wikipedia.org/wiki/Domain_name) (e.g. `torproject.net`) or IP address
space (so reverse DNS, e.g. `38.229.82.0/24` AKA
`82.229.38.in-addr.arpa`) is added to our infrastructure.

## Removing a zone

 * git grep the domain in the `tor-nagios` git repository
 * remove the zone in the `domains` repository
   (`dnsadm@nevii.torproject.org:/srv/dns.torproject.org/repositories/domains`)
 * on nevii, remove the generated zonefiles and keys:

        cd /srv/dns.torproject.org/var/
        mv generated/torproject.fr* OLD-generated/
        mv keys/torproject.fr OLD-KEYS/

 * remove the zone from the secondaries (Netnod and our own
   servers). this means visiting the Netnod web interface for that
   side, and Puppet
   (`modules/bind/templates/named.conf.torproject-zones.erb`) for our
   own
 * the domains will probably be listed in other locations, grep Puppet
   for Apache virtual hosts and email aliases
 * the domains will also probably exist in the `letsencrypt-domains`
   repository

## DS records expiry and renewal

A special case is the rotation of the `DNSKEY` / `DS` records. Those
rotate about once every two years, and require manual operation on the
registrar (currently <https://joker.com>). 

A Nagios hook is in `/srv/dns.torproject.org/bin/dsa-check-and-extend-DS`, and
basically wraps `manage-dnssec-keys` with some Nagios status codes. It
will warn when the key is about to expire and extend it before it
expires (while still flagging a critical warning in Nagios).

To fix this error, you need to [visit joker.com](https://joker.com/) and authenticate
with the password in `hosts-extra-info` in tor-passwords, along with
the 2FA dance. Then:

 1. click on the "modify" button next to the domain affected (was
    first a gear but is now a pen-like icon thing)
 2. find the DNSSEC section
 3. click the "modify" button to edit records
 4. click "more" to add a record

The new key should already be present on the DNS master (currently
`nevii`) in:

    /srv/dns.torproject.org/var/keys/$DOMAIN/dsset

It is in the format (from [rfc4034](https://tools.ietf.org/html/rfc4034)):

    domain IN DS keytag algo type digest

For example:

    torproject.com.  IN DS 28234 8 2 260a11137e3fca013b90da649d50e9c5eb71b814cc1797ea81ee7c91c17b398a; Pub: 2019-05-25 17:40:07;  Act: 2019-05-25 17:40:07;  Inact: 2021-11-16 17:40:07;  Del: 2021-11-16 17:40:07;  Rev: 2021-10-02 17:40:07
    torproject.com.  IN DS 57040 8 2 ebdf81e6b773f243cdee2879f0d12138115d9b14d560276fcd88e9844777d7e3; Pub: 2021-06-13 17:40:07;  Act: 2021-06-13 17:40:07;  Inact: 2023-10-16 17:40:07;  Del: 2023-10-16 17:40:07;  Rev: 2023-09-01 17:40:07

Note that there are *two* keys there: one (the oldest) should already
be in Joker. you need to add the new one.

With the above, you would have the following in Joker:

 * `alg`: 8 ("RSA/SHA-256", [IANA](https://www.iana.org/assignments/dns-sec-alg-numbers/dns-sec-alg-numbers.xhtml), [RFC5702](https://www.rfc-editor.org/rfc/rfc5702.html))
 * `digest`: ebdf81e6b773f243cdee2879f0d12138115d9b14d560276fcd88e9844777d7e3
 * `type`: 2 ("SHA-256", [IANA](https://www.iana.org/assignments/ds-rr-types/ds-rr-types.xhtml), [RFC4509](https://www.rfc-editor.org/rfc/rfc4509.html))
 * `keytag`: 57040

And click "save".

Make sure to update the record in the `tor-puppet.git` repository, in:

    modules/unbound/files/torproject.org.key

Copy the latest `dsset` entry in there. Normally, unbound takes care
of updating that file to chase new versions, but new hosts will need
that new anchor for bootstrapping.

After a little while, you should be able to check if the new DS record
works on [DNSviz.net](http://dnsviz.net/), for example, the [DNSviz.net view of
torproject.net](http://dnsviz.net/d/torproject.net/dnssec/) should be sane.

The changes will take a while (~10 hours?) to trickle out into all
caches, so it might take a while for the Nagios check to return green.

Eventually, Nagios will complain about the old keys, and we can remove
them from the registrar. Make sure to remove the *old* key, not the
new key. Be careful because the web interface might sort the keys in
an unexpected way. Check the keytag and compare with the expiration
specified in the `dsset` file. The Nagios warning that you will see
will look like:

    DNS - security delegations: WARNING: torproject.com (57040,-28234), torproject.net (63619,-53722), torproject.org (33670,-28486)

The `-` entries (e.g. `-28234`) are the ones that should be removed.

Note: this procedure could be automated by talking with the
registrar's API, for example [Joker.com's DMAPI domain modification
API](https://joker.com/faq/content/27/24/en/domain_modify.html) (see also [those docs](https://dmapi.joker.com/docs/DMAPI-ext.txt)). There are also proposals at the
IETF to allow delegation from the parent zone to allow the child zone
to perform those updates on its own.

Further, puppet ships trust anchors for some of our zones to our unbounds.  If
you updated the DS for one of those, update the corresponding file in
`tsa-puppet/modules/unbound/files`.  Existing machines don't need that (since
we do slow, [RFC5011](https://www.rfc-editor.org/rfc/rfc5011.html)-style rolling of KSKs), but new instances will be sad if we
ship them obsolete trust anchors.

### Special case: RFC1918 zones

The above is for public zones, for which we have Nagios checks that
warn us about impeding doom. But we also sign zones about reverse IP
looks, specifically `30.172.in-addr.arpa.` Normally, recursive
nameservers pick new signatures in that zone automatically, thanks to
[rfc 5011](https://tools.ietf.org/html/rfc5011).

But if a new host gets provisionned, it needs to get bootstrapped
somehow. This is done by Puppet, but those records are maintained by
hand and will get out of date. This implies that after a while, you
will start seeing messages like this for hosts that were installed
after the expiration date:

    16:52:39 <nsa> tor-nagios: [submit-01] unbound trust anchors is WARNING: Warning: no valid trust anchors found for 30.172.in-addr.arpa.

The solution is to go on the primary nameserver (currently `nevii`)
and pick the non-revoked DSSET line from this file:

    /srv/dns.torproject.org/var/keys/30.172.in-addr.arpa/dsset

... and inject it in Puppet, in:

    tor-puppet/modules/unbound/files/30.172.in-addr.arpa.key

Then new hosts will get the right key and bootstrap properly. Old
hosts can get the new key by removing the file by hand on the server
and re-running Puppet:

    rm /var/lib/unbound/30.172.in-addr.arpa.key ; puppet agent -t

## Pager playbook

In general, to debug DNS issues, those tools are useful:

 * [DNSviz.net](https://dnsviz.net/), e.g. [a DNSSEC Authentication Chain](https://dnsviz.net/d/82.229.38.in-addr.arpa/dnssec/)
 * `dig`

### unbound trust anchors: Some keys are old

This warning can happen when a host was installed with old keys and
unbound wasn't able to rotate them:

    20:05:39 <nsa> tor-nagios: [chi-node-05] unbound trust anchors is WARNING: Warning: Some keys are old: /var/lib/unbound/torproject.org.key.

The fix is to remove the affected file and rerun Puppet:

    rm /var/lib/unbound/torproject.org.key
    puppet agent --test

### unbound trust anchors: Warning: no valid trust anchors

So this can happen too:

    11:27:49 <nsa> tor-nagios: [chi-node-12] unbound trust anchors is WARNING: Warning: no valid trust anchors found for 30.172.in-addr.arpa.

If this happens on *many* hosts, you will need to update the key, see
the [Special case: RFC1918 zones section](#special-case-rfc1918-zones), above. But if it's a
single host, it's possible it was installed during the window where
the key was expired, and hasn't been properly updated by Puppet
yet. 

Try this:

    rm /var/lib/unbound/30.172.in-addr.arpa.key ; puppet agent -t

Then the warning should have gone away:

    # /usr/lib/nagios/plugins/dsa-check-unbound-anchors
    OK: All keys in /var/lib/unbound recent and valid

If not, see the [Special case: RFC1918 zones section](#special-case-rfc1918-zones) above.

### DNS - zones signed properly is CRITICAL

When adding a new reverse DNS zone, it's possible you get this warning
from Nagios:

    13:31:35 <nsa> tor-nagios: [global] DNS - zones signed properly is CRITICAL: CRITICAL: 82.229.38.in-addr.arpa
    16:30:36 <nsa> tor-nagios: [global] DNS - key coverage is CRITICAL: CRITICAL: 82.229.38.in-addr.arpa

That might be because Nagios thinks this zone should be signed (while
it isn't and cannot). The fix is to add this line to the zonefile:

    ; ds-in-parent = no

And push the change. Nagios should notice and stop caring about the
zone.

In general, this Nagios check provides a good idea of the DNSSEC chain
of a zone:

    $ /usr/lib/nagios/plugins/dsa-check-dnssec-delegation overview 82.229.38.in-addr.arpa
                           zone DNSKEY               DS@parent       DLV dnssec@parent
    --------------------------- -------------------- --------------- --- ----------
         82.229.38.in-addr.arpa                                          no(229.38.in-addr.arpa), no(38.in-addr.arpa), yes(in-addr.arpa), yes(arpa), yes(.)

Notice how the `38.in-addr.arpa` zone is not signed? This zone can
therefore not be signed with DNSSEC.

### DNS - delegation and signature expiry is WARNING

If you get a warning like this:

    13:30:15 <nsa> tor-nagios: [global] DNS - delegation and signature expiry is WARNING: WARN: 1: 82.229.38.in-addr.arpa: OK: 12: unsigned: 0

It might be that the zone is not delegated by upstream. To confirm,
run this command on the Nagios server:

    $ /usr/lib/nagios/plugins/dsa-check-zone-rrsig-expiration  82.229.38.in-addr.arpa
    ZONE WARNING: No RRSIGs found; (0.66s) |time=0.664444s;;;0.000000

On the primary DNS server, you should be able to confirm the zone is
signed:

    dig @nevii  -b 127.0.0.1 82.229.38.in-addr.arpa +dnssec

Check the next DNS server up (use `dig -t NS` to find it) and see if
the zone is delegated:

    dig @ns1.cymru.com 82.229.38.in-addr.arpa +dnssec

If it's not delegated, it's because you forgot step 8 in the zone
addition procedure. Ask your upstream or registrar to delegate the
zone and run the checks again.

### DNS - security delegations is WARNING

This error:

    11:51:19 <nsa> tor-nagios: [global] DNS - security delegations is WARNING: WARNING: torproject.net (63619,-53722), torproject.org (33670,-28486)

... **will** happen after rotating the DNSSEC keys at the
registrar. The trick is then simply to remove those keys, at the
registrar. See [DS records expiry and renewal](#ds-records-expiry-and-renewal) for the procedure.

### DNS SOA sync

An example of this problem is the error:

    Nameserver ns5.torproject.org for torproject.org returns 0 SOAs

That is because the target nameserver (`ns5` in this case) does not
properly respond for the `torproject.org`. To reproduce the error, you
can run this on the Nagios server:

    /usr/lib/nagios/plugins/dsa-check-soas -a nevii.torproject.org torproject.org -v

This happens because the server doesn't correctly transfer the zones
from the master. You can confirm the problem by looking at the logs on
the affected server and on the primary server (e.g. with `journalctl
-u named -f`). Restarting the server will trigger a zone transfer
attempt.

Typically, this is because a change in `tor-puppet.git` was forgotten
(in `named.conf.options` or `named.conf.puppet-shared-keys`).

### DNS - DS expiry

Example:

    2023-08-22 16:34:36 <nsa> tor-nagios: [global] DNS - DS expiry is WARNING: WARN: torproject.com, torproject.net, torproject.org : OK: 4
    2023-08-26 16:25:39 <nsa> tor-nagios: [global] DNS - DS expiry is CRITICAL: CRITICAL: torproject.com, torproject.net, torproject.org : OK: 4

Full status information is, for example:

    CRITICAL: torproject.com, torproject.net, torproject.org : OK: 4
    torproject.com: Key 57040 about to expire.
    torproject.net: Key 63619 about to expire.
    torproject.org: Key 33670 about to expire.

This is Nagios warning you the DS records are about to expire. They
will still be renewed so it's not immediately urgent to fix this, but
eventually the [DS records expiry and renewal](#ds-records-expiry-and-renewal) procedure should be
followed.

The old records that should be replaced are mentioned by Nagios in the
extended status information, above.

## Disaster recovery

<!-- what to do if all goes to hell. e.g. restore from backups? -->
<!-- rebuild from scratch? not necessarily those procedures (e.g. see -->
<!-- "Installation" below but some pointers. -->

# Reference

## Installation

### Secondary name server

To install a secondary nameserver, you first need to create a [new
machine](howto/new-machine), of course. Requirements for this service:

 * trusted location, since DNS is typically clear text traffic
 * DDoS resistant, since those have happened in the past
 * stable location because secondary name servers are registered as
   "glue records" in our zones and those take time to change
 * 2 cores, 2GB of ram and a few GBs of disk should be plenty for now

In the following example, we setup a new secondary nameserver in the
gnt-dal [Ganeti](howto/ganeti) cluster:

 1. create the virtual machine:
 
        gnt-instance add
            -o debootstrap+bullseye
            -t drbd --no-wait-for-sync
            --net 0:ip=pool,network=gnt-dal-01
            --no-ip-check
            --no-name-check
            --disk 0:size=10G
            --disk 1:size=2G,name=swap
            --backend-parameters memory=2g,vcpus=2
            ns3.torproject.org

 2. the rest of the [new machine procedure](howto/new-machine)

 3. add the `bind::secondary` class to the instance in Puppet, also
    add it to `modules/bind/templates/named.conf.options.erb` and
    `modules/bind/templates/named.conf.puppet-shared-keys.erb`
 
 4. generate a tsig secret on the primary server (currently `nevii`):
 
        tsig-keygen

 5. add that secret in Trocla with this command on the Puppet server
    (currently `pauli`):
    
        trocla set tsig-nevii.torproject.org-ns3.torproject.org plain

 6. add the server to the
    `/srv/dns.torproject.org/etc/dns-helpers.yaml` configuration file (!)

 7. regenerate the zone files:
 
        sudo -u dnsadm /srv/dns.torproject.org/bin/update

 8. run puppet on the new server, then on the primary

 9. test the new nameserver:

    At this point, you should be able to resolve names from the
    secondary server, for example this should work:

        dig torproject.org @ns3.torproject.org

    Test some reverse DNS as well, for example:

        dig -x 204.8.99.101 @ns3.torproject.org

    The logs on the primary server should not have too many warnings:

        journalctl -u named -f

 9. once the server is behaving correctly, add it to the glue records:
 
    1. login to `joker.com`
    2. go to "Nameserver"
    3. "Create a new nameserver" (or, if it already exists, "Change"
       it)

Nagios should pick up the changes and the new nameserver
automatically. The affected check is `DNS SOA sync - torproject.org`
and similar, or the `dsa_check_soas_add` check command.

## Upgrades

<!-- how upgrades are performed. preferably automated through Debian -->
<!-- packages, otherwise document how upgrades are performed. see also -->
<!-- the Testing section below -->

## SLA

<!-- this describes an acceptable level of service for this service -->

## Design and architecture

<!-- how this is built -->
<!-- should reuse and expand on the "proposed solution" discussed in -->
<!-- a previous RFC or the Discussion section below. it's a -->
<!-- "as-built" documented, whereas the "Proposed solution" is an -->
<!-- "architectural" document, which the final result might differ -->
<!-- from, sometimes significantly -->

TODO: This needs to be documented better. weasel made a [blog post](https://dsa.debian.org/dsablog/2014/The_Debian_DNS_universe/)
describing parts of the infrastructure on Debian.org, and that is
partly relevant to TPO as well.

## Services

<!-- open ports, daemons, cron jobs -->

## Storage

<!-- databases? plain text file? the frigging blockchain? memory? -->

## Queues

<!-- email queues, job queues, schedulers -->

## Interfaces

<!-- e.g. web APIs, commandline clients, etc -->

## Authentication

<!-- SSH? LDAP? standalone? -->

## Implementation

<!-- programming languages, frameworks, versions, license -->

## Related services

<!-- dependent services (e.g. authenticates against LDAP, or requires -->
<!-- git pushes)  -->

## Issues

<!-- such projects are never over. add a pointer to well-known issues -->
<!-- and show how to report problems. usually a link to the -->
<!-- issue tracker. consider creating a new Label to regroup the -->
<!-- issues if using the general tracker. see also TPA-RFC-19. -->

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search] with the
label ~DNS.

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues?label_name%5B%5D=DNS

## Maintainer

<!-- document who deployed and operates this service, the team and -->
<!-- ideally the person inside that team -->

## Users

<!-- who the main users are, how they use the service. possibly reuse -->
<!-- the Personas section in the RFC, if available. -->

## Upstream

<!-- who the upstreams are, if they are still active, -->
<!-- collaborative, how do we keep up to date, support channels, see -->
<!-- also the "Issues" section above -->

## Monitoring and metrics

<!-- describe how this service is monitored, how security issues and -->
<!-- upgrades are tracked, see also "Upgrades" above. -->

## Tests

<!-- how the service can be tested, for example after major changes -->
<!-- like IP address changes or upgrades. describe CI, test suites, linting -->

## Logs

<!-- where are the logs? how long are they kept? any PII? -->
<!-- what about performance metrics? same questions -->

## Backups

<!-- does this service need anything special in terms of backups? -->
<!-- e.g. locking a database? special recovery procedures? -->

## Other documentation

 * [bind9 reference manual](https://bind9.readthedocs.io/)
 * [DNSSEC debugger](https://dnssec-debugger.verisignlabs.com/)
 * [DNSviz.net](https://dnsviz.net/)

# Discussion

<!-- the "discussion" section is where you put any longer conversation -->
<!-- about the project that you will not need in a casual -->
<!-- review. history of the project, why it was done the way it was -->
<!-- (as opposed to how), alternatives, and other proposals are -->
<!-- relevant here. -->

<!-- this at least partly overlaps with the TPA-RFC process (see -->
<!-- policy.md), but in general should defer to proposals when -->
<!-- available -->

## Overview

<!-- describe the overall project. should include a link to a ticket -->
<!-- that has a launch checklist -->

<!-- if this is an old project being documented, summarize the known -->
<!-- issues with the project. --> 

## Security and risk assessment

<!--

 5. When was the last security review done on the project? What was
    the outcome? Are there any security issues currently? Should it
    have another security review?

 6. When was the last risk assessment done? Something that would cover
    risks from the data stored, the access required, etc.

-->

## Technical debt and next steps

<!--

 7. Are there any in-progress projects? Technical debt cleanup?
    Migrations? What state are they in? What's the urgency? What's the
    next steps?

 8. What urgent things need to be done on this project?

-->

## Proposed Solution

<!-- Link to RFC -->

## Other alternatives

<!-- include benchmarks and procedure if relevant -->

### Debian registrar scripts

Debian has a [set of scripts](https://salsa.debian.org/dsa-team/mirror/dsa-misc/-/tree/master/scripts/dns-providers) to automate talking to some providers
like Netnod. A YAML file has metadata about the configuration, and
pushing changes is as simple as:

    publish tor-dnsnode.yaml

That config file would look something like:

    ---
      endpoint: https://dnsnodeapi.netnod.se/apiv3/
      base_zone:
        endcustomer: "TorProject"
        masters:
          # nevii.torproject.org
          - ip: "49.12.57.130"
            tsig: "netnod-torproject-20180831."
          - ip: "2a01:4f8:fff0:4f:266:37ff:fee9:5df8"
            tsig: "netnod-torproject-20180831."
        product: "probono-premium-anycast"

This is not currently in use at TPO and changes are operated manually
through the web interface.

### zonetool

https://git.autistici.org/ai3/tools/zonetool is a YAML based zone
generator with DNSSEC support.

### no DNSSEC

There's been some critiques of DNSSEC over the years, here are some of
the critiques:

 * [Geoff Huston - DNSSEC: Yes or No](https://ripe86.ripe.net/archives/video/1018/) at [RIPE 86](https://ripe86.ripe.net/) (May 2023), [slides](https://ripe86.ripe.net/presentations/51-2023-05-23-dnssec.pdf)
 * [Matt Brown: Calling time on DNSSEC](https://www.mattb.nz/w/2023/06/02/calling-time-on-dnssec/), following the [.nz
   outage](https://internetnz.nz/news-and-articles/dnssec-chain-validation-issue-for-nz-second-level-domain/) (June 2023)

### automatic DNSSEC management with bind

Right now, the Nagios check (!) is responsible for key rotation and
all that stuff. We could move this in bind directly. Here's how weasel
is currently experimenting with it:

```
 inline-signing yes;

 };
+  dnssec-policy "dnssec-policy-mustelid.at" {
+    keys {
+      // ksk key-directory lifetime P2Y algorithm rsasha256 2048;
+      // zsk key-directory lifetime P4M algorithm rsasha256 2048;
+      ksk key-directory lifetime P4M algorithm rsasha256 2048;
+      zsk key-directory lifetime P50D algorithm rsasha256 1536;
+    };
+
+    dnskey-ttl P1D;
+    max-zone-ttl P1W;
+    nsec3param;
+
+    publish-safety P15D;
+    purge-keys P90D;
+    retire-safety P15D;
+
+    zone-propagation-delay PT8H;
+
+    signatures-refresh P25D;
+    signatures-validity P40D;
+    signatures-validity-dnskey P40D;
+  };
+
 zone "mustelid.at" {
         type master;
         file "/srv/dns.noreply.org/var/generated/mustelid.at";
@@ -506,12 +542,12 @@
  2a04:dd00:21:3::2   ; // ns2.sthu.org/plato.sthu.org

         };
-        key-directory "/srv/dns.noreply.org/var/keys/mustelid.at";
-sig-validity-interval 40 25;
-auto-dnssec maintain;
-inline-signing yes;
+          key-directory "/srv/dns.noreply.org/var/keys/mustelid.at";
+  dnssec-policy "dnssec-policy-mustelid.at";
+  inline-signing yes;
  
 };
 ```
