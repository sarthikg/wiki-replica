---
title: learning how to do an ssh jump host on tpo
---

You need to use an ssh jump host to access internal machines at tpo.
If you have a recent enough ssh (>= 2016 or so), then you can use the `ProxyJump` directive.  Else, use `ProxyCommand`.
`ProxyCommand` automatically executes the ssh command on the host to jump to the next host and forward all traffic through.

With recent ssh versions:

    Host *.torproject.org !ssh.torproject.org !people.torproject.org !gitlab.torproject.org
      ProxyJump ssh.torproject.org

Or with old ssh versions (before OpenSSH 7.3, or Debian 10 "buster"):

    Host *.torproject.org !ssh.torproject.org !people.torproject.org !gitlab.torproject.org
      ProxyCommand ssh -l %r -W %h:%p ssh.torproject.org

Note that there are multiple `ssh`-like aliases that you can use,
depending on your location (or the location of the target host). Right
now there are two:

 * `ssh-dal.torproject.org` - in [Dallas, TX, USA](https://en.wikipedia.org/wiki/Dallas)
 * `ssh-fsn.torproject.org` - in [Falkenstein, Saxony, Germany](https://en.wikipedia.org/wiki/Falkenstein,_Saxony)

The canonical list for this is searching for `ssh` in the `purpose`
field on the [machines database](https://db.torproject.org/machines.cgi). 

> Note: It is perfectly acceptable to run `ping` against the server to
> determine the closest to your location, and you can also run ping
> *from* the server *to* a target server as well. The shortest path
> will be the one that has the lowest *sum* for those two, naturally.

This naming convention was announced in [TPA-RFC-59](policy/tpa-rfc-59-ssh-jump-host-aliases).

If your local username is different from your TPO username, also set
it in your `.ssh/config`:

    Host *.torproject.org
      User USERNAME

Members of TPA might have a different configuration to login as root
by default, but keep their normal user for key services:

    # interact as a normal user with Puppet, LDAP, jump and gitlab servers by default
    Host puppet.torproject.org db.torproject.org ssh.people.torproject.org people.torproject.org gitlab.torproject.org
      User USERNAME

    Host *.torproject.org
      User root

Note that git hosts are not strictly necessary as you should normally
specify a `git@` user in your git remotes, but it's a good practice
nevertheless to catch those scenarios where that might have been
forgotten.

It is also worth keeping the `known_hosts` file in sync to avoid
server authentication warnings. The server's public keys are also
available in DNS. So add this to your `.ssh/config`:

    Host *.torproject.org
      UserKnownHostsFile ~/.ssh/known_hosts.torproject.org
      VerifyHostKeyDNS ask

And keep the `~/.ssh/known_hosts.torproject.org` file up to date by
regularly pulling it from a TPO host, so that new hosts are
automatically added, for example:

    rsync -ctvLP ssh.torproject.org:/etc/ssh/ssh_known_hosts ~/.ssh/known_hosts.torproject.org
