# Howtos

This documentation is primarily aimed at sysadmins and establishes
various procedures not necessarily associated with a specific service.

<!-- update with `ls -d howto/*.md | sed 's/.md$//' | while read page; do if ! grep -q "$page" service.md ; then echo $page ; fi ; done | sed 's,howto/\(.*\), * [\1](howto/\1),' | grep -v wkd | sort` -->

 * [apu](howto/apu)
 * [benchmark](howto/benchmark)
 * [build_and_upload_debs](howto/build_and_upload_debs)
 * [create-a-new-user](howto/create-a-new-user)
 * [cumin](howto/cumin)
 * [fabric](howto/fabric)
 * [incident-response](howto/incident-response)
 * [ipv6](howto/ipv6)
 * [iscsi](howto/iscsi)
 * [keyboard](howto/keyboard)
 * [lektor](howto/lektor)
 * [letsencrypt](howto/letsencrypt)
 * [lvm](howto/lvm)
 * [new-machine-cymru](howto/new-machine-cymru)
 * [new-machine-hetzner-cloud](howto/new-machine-hetzner-cloud)
 * [new-machine-hetzner-robot](howto/new-machine-hetzner-robot)
 * [new-machine](howto/new-machine)
 * [new-machine-mandos](howto/new-machine-mandos)
 * [new-machine-ovh-cloud](howto/new-machine-ovh-cloud)
 * [new-person](howto/new-person)
 * [nftables](howto/nftables)
 * [openpgp](howto/openpgp)
 * [postgresql](howto/postgresql)
 * [quintex](howto/quintex)
 * [raid](howto/raid)
 * [rename-a-host](howto/rename-a-host)
 * [retire-a-host](howto/retire-a-host)
 * [retire-a-user](howto/retire-a-user)
 * [upgrades](howto/upgrades)
 * [yubikey](howto/yubikey)
